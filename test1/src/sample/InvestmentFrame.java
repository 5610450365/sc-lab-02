package sample;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{    
   private static final int FRAME_WIDTH = 450;
   private static final int FRAME_HEIGHT = 100;

     

   public JLabel rateLabel;
   public JTextField rateField;
   public JButton button;
   public JLabel resultLabel;
   public JPanel panel;
  
   
   
  private double DEFAULT_RATE;
   
  public InvestmentFrame(double balance,double DEFAULT_RATE){
	  
	  this.DEFAULT_RATE = DEFAULT_RATE;
	  resultLabel = new JLabel("balance: " + balance);

	   createTextField();
	   createButton();
	   createPanel();
	  setVisible(true);
      setSize(FRAME_WIDTH, FRAME_HEIGHT);
  }

   public void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   public void createButton()
   {
      button = new JButton("Add Interest");
      
   }

   public void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   } 
}
