package sample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

public class control {
	
	private static final double DEFAULT_RATE = 5;
	private static final double INITIAL_BALANCE = 1000; 
	private static double balance;
	BankAccount account ;
	InvestmentFrame inF ;
	
	 public static void main(String[] args){
		 
		   BankAccount account = new BankAccount(INITIAL_BALANCE);
		   balance = account.getBalance();
		   InvestmentFrame inF = new InvestmentFrame(balance,DEFAULT_RATE);
		   
		   
		   class AddInterestListener implements ActionListener
		      {
		         public void actionPerformed(ActionEvent event)
		         {
		            double rate = Double.parseDouble(inF.rateField.getText());
		            double interest = account.getBalance() * rate / 100;
		            account.deposit(interest);
		            inF.resultLabel.setText("balance: " + account.getBalance());
		         }            
		      }
		      
		      ActionListener listener = new AddInterestListener();
		      inF.button.addActionListener(listener);
		   
		   
		   
	 }

}
//attribute because new object account for call attribute of class Bankaccount
//create new object inF  for call attribute of  class InvestmentFrame 